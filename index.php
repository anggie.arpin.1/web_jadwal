<?php

    if(isset($_POST["submit"])){
        $nama = $_POST["namakelas"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Dosen</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="index.php"><img src="img/UNDIKSHA.png" alt="Logo Undiksha" width="50 px"></a>
    <!-- Links -->
    <ul class="navbar-nav">
    <li class="nav-item">
    <a class="nav-link" href="dosen.php">Form Dosen</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="kelas.php">Form Kelas</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="jadwal.php">Form Jadwal</a>
    </li>
    </ul>
</nav>

</body>
</html>